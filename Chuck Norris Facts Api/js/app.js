document.addEventListener('DOMContentLoaded', () =>{

    let imgField = document.getElementById('norris-img');
    let txtField = document.getElementById('norris-txt');
    let refreshBtn = document.getElementById('refresh');

    refreshBtn.addEventListener('click',getDataFromAPI);
    
    function getDataFromAPI() {

        fetch('https://api.chucknorris.io/jokes/random')
        .then(function(resp){return resp.json()})
        .then(function(data){
            console.log(data);
            console.log(data.value);
            console.log(data.icon_url);
            drawToView(data.icon_url,data.value);
        })
        .catch(function(){

        });
    }

    function drawToView(img,txt){
        imgField.src= img;
        txtField.innerText = txt;
    }
    // Initial Call to API
    getDataFromAPI();
});