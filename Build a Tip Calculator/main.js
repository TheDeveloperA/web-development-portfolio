function calculateClicked(){

    // Get values from elements
    var amount = document.getElementById("amount").value;
    var service = document.getElementById("service").value;
    var sharingBill = document.getElementById("shared").value;
    // Check if form is valid
    if(isFormCompleted(amount,sharingBill)){
        let tip = determineTipValue(service);
        let total = calculateTotalWithTips(amount,tip);
        total /= sharingBill;
        // create element for total
        document.getElementById("value").innerHTML = "£" + total.toString() + " per person";
    }
}

function isFormCompleted(amount, sharingBill){
    if(amount.toString().trim() != "" && sharingBill.toString().trim() != "" ){
        return true;
    }else{
        return false;
    }
}

function calculateTotalWithTips(amount, service){
    return ((amount / 100) * (100 + service)); 
}

function determineTipValue(service){
    switch(service){
        case "Outstanding":
            return 25;
        case "Good":
            return 15;
        case "Normal":
            return 10;
        case "Poor":
            return 0;
        default:
            return 0;
    }
}