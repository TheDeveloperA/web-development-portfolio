document.addEventListener('DOMContentLoaded', () => {

    let addBtn = document.querySelector('button');
    addBtn.addEventListener('click',addToDo);

    function addToDo(){
        let input = document.getElementById('todo');

        // Create a new li element and add to ul
        let newEntry = document.createElement('li');
        newEntry.innerText = input.value;
        document.querySelector('ul').appendChild(newEntry);

        // Clear input box when new item added
        input.value = "";
    }
});