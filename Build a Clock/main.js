function showTime(){
    let today = new Date();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return time;
}

let timeElement = document.getElementById("time");
timeElement.textContent = showTime();